﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using ChaosConstructions.Models;
using System.Data.SqlClient;
using System.Text;

namespace ChaosConstructions.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
            await ShowCard(context, GetStartCard);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;
            await ShowAnnuvalConferenceTicket(context);
        }

        private static Attachment GetStartCard()
        {
            var heroCard = new HeroCard
            {
                Title = "ChaosConstructions 2018",
                Subtitle = "Компьютерный фестиваль",
                Text = "25-26 августа, Санкт-Петербург, Конгресс-Центр ЛПМ",
                Images = new List<CardImage> { new CardImage("https://chaosconstructions.ru/media/images/cc2018/cc2018.png") },
                Buttons = new List<CardAction> { new CardAction(ActionTypes.OpenUrl, "Сайт", value: "https://chaosconstructions.ru/") }
            };

            return heroCard.ToAttachment();
        }

        private static Attachment GetMapCard()
        {
            var heroCard = new HeroCard
            {
                Title = "ChaosConstructions 2018",
                Subtitle = "Карта фестиваля",
                Text = "25-26 августа, Санкт-Петербург, Конгресс-Центр ЛПМ",
                Images = new List<CardImage> { new CardImage("https://it-events.com/system/ckeditor/pictures/19255/content_mapa_2018.png") },
                Buttons = new List<CardAction> { new CardAction(ActionTypes.OpenUrl, "Сайт", value: "https://chaosconstructions.ru/events") }
            };

            return heroCard.ToAttachment();
        }

        private static Attachment GetTimeCard()
        {
            var heroCard = new HeroCard
            {
                Title = "ChaosConstructions 2018",
                Subtitle = "Расписание фестиваля",
                Text = "25-26 августа, Санкт-Петербург, Конгресс-Центр ЛПМ",
                Buttons = new List<CardAction> { new CardAction(ActionTypes.OpenUrl, "Расписание", value: "https://chaosconstructions.ru/prog") }
            };

            return heroCard.ToAttachment();
        }

        private static List<string> arr = new List<string>{"Карта фестиваля.", "Расписание фестиваля.", "Куда пойти?"/*, "Во сколько следующий показ конкурсных работ?"*/};

        private Task ShowAnnuvalConferenceTicket(IDialogContext context)
        {
            PromptDialog.Choice<string>(
                context: context,
                resume: ChoiceReceivedAsync,
                options: arr,
                prompt: "Что Вы хотите сделать?",
                retry: "Что-то пошло не так. Попробуйте снова, пожалуйста.",
                promptStyle: PromptStyle.Keyboard
                );

            return Task.CompletedTask;
        }

        private delegate Attachment CardDelegate();

        private async Task ShowCard(IDialogContext context, CardDelegate method)
        {
            var message = context.MakeMessage();
            var attachment = method();
            message.Attachments.Add(attachment);
            await context.PostAsync(message);
        }

        private async Task GetEvents(IDialogContext context)
        {
            string show = String.Empty;
            DateTime nowTime = (DateTime.UtcNow).AddHours(3);
            TimeTable.CreateTimeTable(context);
            foreach (var i in TimeTable.Events)
            {
                if(i.TimeStart <= nowTime && i.TimeFinish > nowTime)
                {
                show += i.ToString() + "\n***\n";
                }
            }
            if(show == String.Empty)
            {
                if (nowTime < new DateTime(2018, 8, 25, 11, 0, 0))
                {
                    await context.PostAsync("Открытие фестиваля в 11:00!");
                }
                else
                {
                    await context.PostAsync("Мероприятия закончились:(");
                }
            }
            else
            {
                await context.PostAsync(show);
            }
        }

        /*private async Task GetCompetitionTime(IDialogContext context)
        {
            string show = String.Empty;
            DateTime nowTime = (DateTime.UtcNow).AddHours(3);
            DateTime curent;
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "chaosconstructions.database.windows.net";
                builder.UserID = "Xottabich";
                builder.Password = "Polly18042000";
                builder.InitialCatalog = "ChaosConstructionsDB";
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT * FROM Competition;");
                    String sql = sb.ToString();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                curent = new DateTime(2018, 7, reader.GetInt32(1), reader.GetInt32(2), reader.GetInt32(3), 0);
                                if (nowTime <= curent)
                                {
                                    var minute = curent.Minute < 10 ? String.Format("0{0}", curent.Minute) : String.Format("{0}", curent.Minute);
                                    show += String.Format("В {0}:{1}.\n", curent.Hour, minute);
                                    TimeSpan cur = curent.Subtract(nowTime);
                                    show += String.Format("Осталось {0} минут.", Math.Round(cur.TotalMinutes));
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                await context.PostAsync("Проблемы с сервером.");
            }

            if (show == String.Empty)
            {
                await context.PostAsync("Я не знаю:(");
            }
            else
            {
                await context.PostAsync(show);
            }
        }*/

        private async Task ChoiceReceivedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            switch ((await activity).ToString())
            {
                case "Карта фестиваля.":
                    await ShowCard(context, GetMapCard);
                    break;
                case "Расписание фестиваля.":
                    await ShowCard(context, GetTimeCard);
                    break;
                case "Куда пойти?":
                    await GetEvents(context);
                    break;
                /*case "Во сколько следующий показ конкурсных работ?":
                    await GetCompetitionTime(context);
                    break;*/
                default:
                    await ShowAnnuvalConferenceTicket(context);
                    break;
            }
            await ShowAnnuvalConferenceTicket(context);
        }
    }
}