﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaosConstructions.Models
{
    public class TimeTable
    {
        public static List<Event> Events = new List<Event>();

        public static void CreateTimeTable(IDialogContext context)
        {
            Events.Clear();
            /*Events.Add(new Event(new DateTime(2018, 7, 23, 8, 40, 0), new DateTime(2018, 7, 23, 9, 00, 0), "Открытие", "Сцена"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 9, 00, 0), new DateTime(2018, 7, 23, 10, 30, 0), "Мероприятие 1", "Зал 1"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 9, 00, 0), new DateTime(2018, 7, 23, 10, 30, 0), "Мероприятие 2", "Зал 2"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 9, 00, 0), new DateTime(2018, 7, 23, 10, 30, 0), "Мероприятие 3", "Зал 3"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 10, 30, 0), new DateTime(2018, 7, 23, 13, 30, 0), "Мероприятие 4", "Зал 1"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 10, 40, 0), new DateTime(2018, 7, 23, 13, 40, 0), "Мероприятие 5", "Зал 2"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 10, 30, 0), new DateTime(2018, 7, 23, 19, 30, 0), "Мероприятие 6", "Зал 3"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 13, 30, 0), new DateTime(2018, 7, 23, 19, 30, 0), "Мероприятие 7", "Зал 1"));
            Events.Add(new Event(new DateTime(2018, 7, 23, 13, 40, 0), new DateTime(2018, 7, 23, 20, 00, 0), "Мероприятие 8", "Зал 2"));*/
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "chaosconstruction.database.windows.net";
                builder.UserID = "Xottabich";
                builder.Password = "AnilisAnilop18042000";
                builder.InitialCatalog = "ChaosConstructions";
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT * FROM Events;");
                    String sql = sb.ToString();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Events.Add(new Event(new DateTime(2018, 8, reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), 0), new DateTime(2018, 8, reader.GetInt32(3), reader.GetInt32(4), reader.GetInt32(5), 0), reader.GetString(6), reader.GetString(7)));
                            }
                        }
                    }
                }
            }
            catch (SqlException)
            {
                context.PostAsync("Проблемы с сервером:(");
            }
        }
    }
}
