﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaosConstructions.Models
{
    public class Event
    {
        public DateTime TimeStart
        {
            get;
        }

        public DateTime TimeFinish
        {
            get;
        }

        public string Name
        {
            get;
        }

        public string Place
        {
            get;
        }

        public Event(DateTime timeStart, DateTime timeFinish, string name, string place)
        {
            TimeStart = timeStart;
            TimeFinish = timeFinish;
            Name = name;
            Place = place;
        }

        public override string ToString()
        {
            var minute1 = TimeStart.Minute < 10 ? String.Format("0{0}", TimeStart.Minute) : String.Format("{0}", TimeStart.Minute);
            var minute2 = TimeFinish.Minute < 10 ? String.Format("0{0}", TimeFinish.Minute) : String.Format("{0}", TimeFinish.Minute);

            return String.Format("{0}:{1} - {2}:{3}\n{4}\n{5}.", TimeStart.Hour, minute1, TimeFinish.Hour, minute2, Name, Place);
        }
    }
}
